# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'pre-packager/version'

Gem::Specification.new do |spec|
  spec.name          = "pre-packager"
  spec.version       = App::Creator::VERSION
  spec.authors       = ["Gonzalo Matheu"]
  spec.email         = ["gonzalommj@gmail.com"]
  spec.summary       = %q{Creates directory structure for linux applications}
  spec.description   = %q{Provides a DSL to create directories, files and links structure from different sources (e.g: tar.gz, templates)}
  spec.homepage      = "https://gitlab.com/gmatheu/pre-packager"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "guard"
  spec.add_development_dependency "guard-rspec"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "pry-nav"
  spec.add_development_dependency "webmock"
  spec.add_development_dependency "fpm"
end
