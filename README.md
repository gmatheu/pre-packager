# Pre-Packager
[![Gem Version](https://badge.fury.io/rb/pre-packager.svg)](http://badge.fury.io/rb/pre-packager)
[![CI](https://magnum-ci.com/status/42b45d01d68f077a9728a6d4d1f4ec37.png)](https://magnum-ci.com/projects/2192)

Creates directory structure for linux applications

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'pre-packager'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install pre-packager

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it (https://gitlab.com/gmatheu/pre-packager.git)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
