
require 'spec_helper'

require 'open-uri'

module Executor
  def execute cmd
    cmd
  end
end

RSpec.describe Application do
  describe "#download_from" do
    let(:file) { 'file.tar.gz'}
    let(:url) { "http://url/#{file}" }
    subject { Application.new name: ''}

    before :each do
      stub_request(:get, url)
    end

    context 'just download' do
      it 'returns file path' do
        path = subject.download_from url
        expect(path).to eq file
      end
    end
  end

  context 'interpolation' do
    describe "#download_from" do
      let(:file) { 'file-$version.tar.gz'}
      let(:filename) { 'file-X.tar.gz' }
      let(:url) { "http://url/#{file}" }
      subject { Application.new name: 'name', version: 'X' }

      before :each do
        stub_request(:get, url)
      end

      context 'just download' do
        it 'returns file path' do
          path = subject.download_from url
          expect(path).to eq filename
        end
      end
    end
  end
end
