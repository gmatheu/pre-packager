require 'spec_helper'

RSpec.describe Interpolator do
  describe "#value_for" do
    context 'no interpolation' do
      let(:value) { "variable" }
      subject { Interpolator.new.value_for value }

      it { is_expected.to eql value }
    end

    context 'interpolate a simple var' do
      let(:val) { 'VAL' }
      let(:var) { 'variable$val' }
      let(:interpolator) { Interpolator.new val: val}
      subject { interpolator.value_for var }

      it { is_expected.to eql "variable#{val}"}
    end
  end
end
