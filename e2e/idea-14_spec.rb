require 'spec_helper'
require 'shell'

def in_dir
  Dir.chdir 'e2e' do
    yield
  end
end
def in_target
  in_dir do
    Dir.chdir 'ideaIC-14' do
      yield
    end
  end
end
RSpec.describe 'Idea IC 14' do
  let(:file) {'ideaIC-14.0.2.tar.gz'}
  let(:target) {'ideaIC-14'}

  after :each do
    # in_dir { File.delete file if File.exists? file }
    # in_dir { FileUtils.rm_rf target if File.exists? target }
  end

  context 'downloading file' do
    in_dir { require './idea-14.rb' }

    it 'downloads file' do
      in_dir { expect(File.exists?(file)).to be_truthy }
    end
    it 'creates temp dir structure' do
      in_target { expect(Pathname.new('opt/ideaIC-14').directory?).to be_truthy }
    end
    it 'creates symlink in path' do
      in_target do
        link = 'usr/local/bin/ideaIC-14'
        expect(Pathname.new(link).exist?).to be_truthy
        expect(Pathname.new(link).symlink?).to be_truthy
        expect(File.readlink(link)).to eql '/opt/ideaIC-14/bin/idea.sh'
      end
    end
    it 'copies templates in given directory' do
      in_target { expect(Pathname.new('usr/local/share/applications').directory?).to be_truthy }
      in_target { expect(File.exists?('usr/local/share/applications/idea-ic-14.desktop')).to be_truthy }
    end
  end
end
