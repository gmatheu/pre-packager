require 'logger'
require 'uri'
require 'fileutils'
require 'pathname'

require 'pre-packager/version'
require 'pre-packager/interpolator'

module PrePackager
  class Logger < Logger
    def initialize
      super STDOUT
      self.formatter = proc do |severity, datetime, progname, msg|
        "#{severity}: #{msg}\n"
      end
    end
  end

  module Executor
    @@LOGGER = Logger.new
    def execute cmd
      @@LOGGER.info "Executing: #{cmd}"
      out = `#{cmd}`
      if $? != 0
        @@LOGGER.info "Output: #{out}"
      end
    end
  end

  class ParamsAssertion
    def initialize *values
      @values = values
    end

    def included? opts
      @values.each do |v|
        raise ArgumentError.new "Should contain option: #{v}" unless opts.key? v
      end
    end
  end

  class Content
    include Executor
    @@LINK_BASE_DEFAULT = '/usr/local/bin'

    def initialize context
      @context = context
    end

    def link file, options
      link_base = File.join @context.root, @@LINK_BASE_DEFAULT
      FileUtils.mkdir_p link_base

      target = File.join @context.base, @context.name, file
      linkname = Pathname.new(file).basename.to_s
      linkname = @context.name if options[:use_name]
      create_link = proc { |name| File.join link_base, name}

      execute "ln -sf #{target} #{create_link.call(linkname)}"
      execute("ln -sf #{linkname} #{create_link.call(options[:alias])}") if options[:alias]
      # File.symlink File.join(file), File.join(@path, @@LINK_BASE_DEFAULT, link)
    end
  end

  class Package
    include Executor
    @@LOGGER = Logger.new

    def initialize path, interpolator=Interpolator.new
      @interpolator = interpolator
      @file = path
    end

    class Context
      attr_reader :name, :base, :root, :local_base

      def initialize base, interpolator, opts
        @base = base
        @interpolator = interpolator

        real_target = "./$name#{base}"
        real_target = "#{real_target}/$name" if opts[:use_name]
        @local_base = @interpolator.value_for real_target

        @name = @interpolator.value_for '$name'
        @root = File.join @local_base, '..', '..'
      end
    end

    def extract target, opts
      ctx = Context.new target, @interpolator, opts

      strip = opts[:strip] || 0
      FileUtils.mkdir_p ctx.local_base
      execute "tar -xf #{@file} -C #{ctx.local_base} --strip #{strip}"

      yield Content.new ctx
    end
  end

  class Application
    include Executor

    @@VALID_FIELDS = [:description, :maintainer]
    @@LOGGER = Logger.new

    def initialize values={}
      ParamsAssertion.new(:name).included? values
      @interpolator = Interpolator.new values
    end

    def download_from url
      uri = URI.parse interpolate(url)
      file = File.basename uri.path

      @@LOGGER.info "Downloading #{uri}"
      execute "wget -nc #{uri}"

      if block_given?
        yield Package.new file, @interpolator
        self
      else
        file
      end
    end

    def with_templates from, to
      FileUtils.mkdir_p interpolate("$name/#{to}")
      target = interpolate("$name/#{to}")
      Dir.glob(from).each {|f| FileUtils.cp_r f, target}

      self
    end

    def create_package opts
      ParamsAssertion.new(:version).included? opts
      name = interpolate '$name'
      paths = Pathname.new(name).
        children.
        map(&:basename).
        map(&:to_s).
        join ' '
      valid_fields = Proc.new { |k, v| @@VALID_FIELDS.include? k}
      compose_field = Proc.new { |k, v| %Q|--#{k} "#{v}"|}
      fields = opts.
        select(&valid_fields).
        map(&compose_field).
        join ' '
      cmd = interpolate %Q|
  fpm -s dir -t deb -C #{name} -n #{name} -v #{opts[:version]} -a all #{fields} #{paths}
      |
      if opts[:dry_run]
        @@LOGGER.info "Command to generate package: #{cmd}"
      else
        execute cmd
      end
    end

    private def interpolate var
      @interpolator.value_for var
    end
  end
end
