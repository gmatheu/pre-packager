require 'pre-packager/interpolator'

module PrePackager
  class Interpolator
    DEFAULT_CHARACTER='$'

    def initialize vars = {}
      @variables = vars
    end

    def value_for var
      @variables.reduce(var) {
        |acc, (k, v)| acc.gsub "#{DEFAULT_CHARACTER}#{k}", v
      }
    end
  end
end
